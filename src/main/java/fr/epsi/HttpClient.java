package fr.epsi;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class HttpClient {

	private static final String API_BASE_URL = "http://localhost:8080/api/adhesion";
	private static final String URL_CREATION_ADHESION = API_BASE_URL + "/create";
	private static final String URL_DELETION_ADHESION = API_BASE_URL + "/delete";

	private static final String USER_MAIL = "david.gayerie.epsi@mailoo.org";

	public static void main(String[] args) throws Exception {
		Client client = ClientBuilder.newClient();

		createAdhesion(client);
		deleteAdherent(client);
	}

	private static void createAdhesion(Client client) throws Exception {
		Adhesion adhesion = new Adhesion();
		adhesion.setEmail(USER_MAIL);
		adhesion.setMotDePasse("12345678");
		
		WebTarget target = client.target(URL_CREATION_ADHESION);
		Response response = target.request().post(Entity.entity(adhesion, MediaType.APPLICATION_JSON));
		if (response.getStatus() != 201) {
			throw new Exception("Erreur lors de la création d'une adhésion. Code de status 201 attendu mais " + response.getStatus() + " reçu !");
		}
	}

	private static void deleteAdherent(Client client) throws Exception {
	    Adhesion adhesion = new Adhesion();
	    adhesion.setEmail(USER_MAIL);

		WebTarget target = client.target(URL_DELETION_ADHESION);
		Response response = target.request().post(Entity.entity(adhesion, MediaType.APPLICATION_JSON));
		if(response.getStatus() != 200) {
		    throw new Exception("Erreur lors de la suppression d'une adhésion. Code de status 200 attendu mais " + response.getStatus() + " reçu !");
        }

	}

}